package com.rak.company.mycompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rak.company.mycompany.entity.Company;
import com.rak.company.mycompany.entity.Department;
import com.rak.company.mycompany.service.CompanyService;

@Controller
@RequestMapping("department")
public class DepartmentController {

private CompanyService companyService;
	
	@Autowired
	public DepartmentController(CompanyService companyService) {
		this.companyService = companyService;
	}
	
	@GetMapping("/list")
	public String listCompany(Model theModel)
	{
		List<Department>listDepartments = companyService.findAllDepartment();
		theModel.addAttribute("departments", listDepartments);
		System.out.println(listDepartments);
		return "department/list_department";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel)
	{
		Department theDepartment = new Department();
		theModel.addAttribute("department",theDepartment);
		return "department/department-form";
	}
	
	@PostMapping("/save")
	public String saveDepartment(@ModelAttribute("department")Department theDepartment,Model theModel)
	{
		System.out.println(theDepartment.toString());
		String response = companyService.saveDepartment(theDepartment);
		theModel.addAttribute("response",response);
		List<Department>listDepartments = companyService.findAllDepartment();
		theModel.addAttribute("departments", listDepartments);
		return "department/list_department";
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("departmentCode") int departmentCode,Model theModel)
	{
		Department theDepartment = companyService.selectDepartmentById(departmentCode);
		theModel.addAttribute("department",theDepartment);
		return "department/department-form";
	}
	
	@GetMapping("/delete")
	public String deleteDepartment(@RequestParam("departmentCode") int departmentCode,Model theModel)
	{
		String response = companyService.deleteCompany(departmentCode);
		theModel.addAttribute("response",response);
		List<Department>listDepartments = companyService.findAllDepartment();
		theModel.addAttribute("departments", listDepartments);
		return "department/list_department";
	}
}
