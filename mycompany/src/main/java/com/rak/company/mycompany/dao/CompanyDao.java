package com.rak.company.mycompany.dao;

import java.util.List;

import com.rak.company.mycompany.entity.Company;
import com.rak.company.mycompany.entity.Department;
import com.rak.company.mycompany.entity.Employee;

public interface CompanyDao {

	public List<Company> findAllCompany();
	public void saveCompany(Company theCompany);
	public void deleteCompany(int id);
	public Company selectCompanyById(int companyCode);
	
	public List<Department> findAllDepartment();
	public void saveDepartment(Department theDepartment);
	public void deleteDepartment(int id);
	public Department selectDepartmentById(int departmentCode);
	
	public List<Employee> findAllEmployee();
	public void saveEmployee(Employee theEmployee);
	public void deleteEmployee(int id);
	public Employee selectEmployeeById(int employeeCode);
}
