package com.rak.company.mycompany.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "department")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "department_code")
	private int departmentCode;
	
	@Column(name = "department_name")
	private String departmentName;
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "company_code")
	private Company company;
	
	@OneToMany(mappedBy = "department",cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	private List<Employee>employees;
	
	
	public Department() {

	}
	
	public Department(String departmentName, Company company, List<Employee> employees) {
		this.departmentName = departmentName;
		this.company = company;
		this.employees = employees;
	}


	public void add(Employee theEmployee)
	{
		if(employees == null)
		{
			employees = new ArrayList<Employee>();
		}
		employees.add(theEmployee);
		
		theEmployee.setDepartment(this);
	}

	public int getDepartmentCode() {
		return departmentCode;
	}


	public void setDepartmentCode(int departmentCode) {
		this.departmentCode = departmentCode;
	}


	public String getDepartmentName() {
		return departmentName;
	}


	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	
}
