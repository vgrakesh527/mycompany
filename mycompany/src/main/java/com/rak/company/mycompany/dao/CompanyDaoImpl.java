package com.rak.company.mycompany.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rak.company.mycompany.entity.Company;
import com.rak.company.mycompany.entity.Department;
import com.rak.company.mycompany.entity.Employee;

@Repository
public class CompanyDaoImpl implements CompanyDao {

	private EntityManager  entityManager;
	
	@Autowired
	public CompanyDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	@Override
	public List<Company> findAllCompany() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Company> theQuery = currentSession.createQuery("from Company",Company.class);
		return theQuery.getResultList();
	}
	@Override
	public void saveCompany(Company theCompany) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theCompany);
	}
	@Override
	public void deleteCompany(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Company> theQuery = currentSession.createQuery("Delete from Company where id = :companyCode");
		theQuery.setParameter("companyCode", id);
		theQuery.executeUpdate();
	}

	@Override
	public List<Department> findAllDepartment() {
		
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Department> theQuery = currentSession.createQuery("from Department",Department.class);
		return theQuery.getResultList();
	}
	@Override
	public Company selectCompanyById(int companyCode) {
		Session currentSession = entityManager.unwrap(Session.class);
		Company theCompany = currentSession.get(Company.class, companyCode);
		return theCompany;
	}
	@Override
	public void saveDepartment(Department theDepartment) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theDepartment);
		
	}
	@Override
	public void deleteDepartment(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Department> theQuery = currentSession.createQuery("Delete from Department where id = :id");
		theQuery.setParameter("id", id);
		theQuery.executeUpdate();
		
	}
	@Override
	public Department selectDepartmentById(int departmentCode) {
		Session currentSession = entityManager.unwrap(Session.class);
		Department theDepartment = currentSession.get(Department.class, departmentCode);
		return theDepartment;
	}
	@Override
	public List<Employee> findAllEmployee() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("from Employee",Employee.class);
		return theQuery.getResultList();
	}
	@Override
	public void saveEmployee(Employee theEmployee) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(theEmployee);
		
	}
	@Override
	public void deleteEmployee(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Employee> theQuery = currentSession.createQuery("Delete from Employee where id = :id");
		theQuery.setParameter("id", id);
		theQuery.executeUpdate();
		
	}
	@Override
	public Employee selectEmployeeById(int employeeCode) {
		Session currentSession = entityManager.unwrap(Session.class);
		Employee theEmployee = currentSession.get(Employee.class, employeeCode);
		return theEmployee;
	}
	
}
