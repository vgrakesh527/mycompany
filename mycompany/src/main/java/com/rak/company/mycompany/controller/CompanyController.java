package com.rak.company.mycompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rak.company.mycompany.entity.Company;
import com.rak.company.mycompany.service.CompanyService;

@Controller
@RequestMapping("company")
public class CompanyController {

	private CompanyService companyService;
	
	@Autowired
	public CompanyController(CompanyService companyService) {
		this.companyService = companyService;
	}

	@GetMapping("/hello")
	public String hello(Model theModel)
	{
		theModel.addAttribute("theDate", new java.util.Date());
		return "helloworld";
	}
	
	@GetMapping("/list")
	public String listCompany(Model theModel)
	{
		List<Company>listCompany = companyService.findAllCompany();
		theModel.addAttribute("companies", listCompany);
		System.out.println(listCompany);
		return "company/list_company";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel)
	{
		Company theCompany = new Company();
		theModel.addAttribute("company",theCompany);
		return "company/company-form";
	}
	
	@PostMapping("/save")
	public String saveCompany(@ModelAttribute("company")Company theCompany)
	{
		companyService.saveCompany(theCompany);
		return "redirect:list";
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("companyCode") int companyCode,Model theModel)
	{
		Company theCompany = companyService.selectCompanyById(companyCode);
		theModel.addAttribute("company",theCompany);
		return "company/company-form";
	}
	
	@GetMapping("/delete")
	public String deleteCompany(@RequestParam("companyCode") int companyCode,Model theModel)
	{
		String response = companyService.deleteCompany(companyCode);
		theModel.addAttribute("response",response);
		List<Company>listCompany = companyService.findAllCompany();
		theModel.addAttribute("companies", listCompany);
		return "company/list_company";
	}
}
