package com.rak.company.mycompany.service;

import java.util.List;

import com.rak.company.mycompany.entity.Company;
import com.rak.company.mycompany.entity.Department;
import com.rak.company.mycompany.entity.Employee;

public interface CompanyService {
	
	public List<Company> findAllCompany();

	public void saveCompany(Company theCompany);

	public List<Department> findAllDepartment();

	public Company selectCompanyById(int companyCode);

	public String deleteCompany(int companyCode);
	
	
	public String saveDepartment(Department theDepartment);
	public String deleteDepartment(int id);
	public Department selectDepartmentById(int departmentCode);
	
	public List<Employee> findAllEmployee();
	public String saveEmployee(Employee theEmployee);
	public String deleteEmployee(int id);
	public Employee selectEmployeeById(int employeeCode);
}
