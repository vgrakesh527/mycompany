package com.rak.company.mycompany.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rak.company.mycompany.entity.Employee;
import com.rak.company.mycompany.service.CompanyService;

@Controller
@RequestMapping("/employees")
public class EmployeesController {
private CompanyService companyService;
	
	@Autowired
	public EmployeesController(CompanyService companyService) {
		this.companyService = companyService;
	}
	
	@GetMapping("/list")
	public String listCompany(Model theModel)
	{
		List<Employee>listEmployees = companyService.findAllEmployee();
		theModel.addAttribute("employees", listEmployees);
		System.out.println(listEmployees);
		return "employee/list_employee";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel)
	{
		Employee theEmployee = new Employee();
		theModel.addAttribute("employee",theEmployee);
		return "employee/employee-form";
	}
	
	@PostMapping("/save")
	public String saveEmployee(@ModelAttribute("employee")Employee theEmployee,Model theModel)
	{
		System.out.println(theEmployee.toString());
		String response = companyService.saveEmployee(theEmployee);
		theModel.addAttribute("response",response);
		List<Employee>listEmployees = companyService.findAllEmployee();
		theModel.addAttribute("employees", listEmployees);
		return "employee/list_employee";
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("employeeCode") int employeeCode,Model theModel)
	{
		Employee theEmployee = companyService.selectEmployeeById(employeeCode);
		theModel.addAttribute("employee",theEmployee);
		return "employee/employee-form";
	}
	
	@GetMapping("/delete")
	public String deleteEmployee(@RequestParam("employeeCode") int employeeCode,Model theModel)
	{
		String response = companyService.deleteCompany(employeeCode);
		theModel.addAttribute("response",response);
		List<Employee>listEmployees = companyService.findAllEmployee();
		theModel.addAttribute("employees", listEmployees);
		return "employee/list_employee";
	}

}
