package com.rak.company.mycompany.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rak.company.mycompany.dao.CompanyDao;
import com.rak.company.mycompany.entity.Company;
import com.rak.company.mycompany.entity.Department;
import com.rak.company.mycompany.entity.Employee;

@Service
public class CompanyServiceImp implements CompanyService {

	private CompanyDao companyDao;
	
	@Autowired
	public CompanyServiceImp(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}


	@Override
	@Transactional
	public List<Company> findAllCompany() {
		return companyDao.findAllCompany();
	}

	@Override
	@Transactional
	public void saveCompany(Company theCompany)
	{
		companyDao.saveCompany(theCompany);
	}
	
	@Override
	@Transactional
	public List<Department> findAllDepartment() {
		return companyDao.findAllDepartment();
	}


	@Override
	@Transactional
	public Company selectCompanyById(int companyCode) {
		return companyDao.selectCompanyById(companyCode);
	}


	@Override
	@Transactional
	public String deleteCompany(int companyCode) {
		
	Company theCompany = selectCompanyById(companyCode);
	System.out.println("Department" + theCompany.getDepartments());
		if(theCompany.getDepartments() == null || (theCompany.getDepartments().isEmpty()))
		{
			companyDao.deleteCompany(companyCode);
			return theCompany.getCompanyName() + " Deleted Successfully";
			
		}
		return "Please Delete Department First";
		
	}
	
	@Override
	@Transactional
	public String saveDepartment(Department theDepartment) {
		Company theCompany = selectCompanyById(theDepartment.getCompany().getCompanyCode());
		if(theCompany == null)
		{
			return "Invalid Company Code";
		}
		theDepartment.setCompany(theCompany);
		companyDao.saveDepartment(theDepartment);
		return "Saved Successfully";
		
	}
	@Override
	@Transactional
	public String deleteDepartment(int id) {
		companyDao.deleteDepartment(id);
		return "";
		
	}


	@Override
	@Transactional
	public Department selectDepartmentById(int departmentCode) {
		return companyDao.selectDepartmentById(departmentCode);
	}


	@Override
	@Transactional
	public List<Employee> findAllEmployee() {
		return companyDao.findAllEmployee();
	}


	@Override
	@Transactional
	public String  saveEmployee(Employee theEmployee) {
		companyDao.saveEmployee(theEmployee);
		return "";
		
	}


	@Override
	@Transactional
	public String deleteEmployee(int id) {
		companyDao.deleteEmployee(id);
		return "";
	}


	@Override
	@Transactional
	public Employee selectEmployeeById(int employeeCode) {
		return companyDao.selectEmployeeById(employeeCode);
	}
}
